//
//  drawListViewController.swift
//  Prize Bond
//
//  Created by Avialdo on 07/01/2017.
//  Copyright © 2017 Avialdo. All rights reserved.
//

import UIKit

class drawListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var reuseIdentifier : String!
    var bond = [String]()
    var draw = [String]()
    var date = [String]()
    
    @IBOutlet weak var drawScheduleTableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        reuseIdentifier = "cell"
        
        bond = ["Rs. 15,000","Rs. 750" ,"Rs. 25,000", "Rs. 7,500","Rs. 1,500","Rs.100","Rs. 40,000","Rs. 200", "Rs. 15,000","Rs. 750","Rs. 7,500" ,"Rs. 25,000" ,"Rs. 1,500","Rs.100","Rs. 40,000" ,"Rs. 200","Rs. 15,000","Rs. 750","Rs. 7,500","Rs. 25,000","Rs. 1,500","Rs.100","Rs. 40,000","Rs. 200","Rs. 15,000" , "Rs. 750","Rs. 25,000","Rs. 7,500","Rs.100" ,"Rs. 1,500" ,"Rs. 40,000","Rs. 200"]
        
        draw = ["69","69","69","69","69","69","69" ,"69","70","70","70","70","70","70","70","70","71","71","71","71","71","71","71","71","72","72","72","72","72","72","72","72"]
        
        date = ["January, 02 2017" ,"January, 16 2017" ,"February, 01 2017", "February, 01 2017","February, 15 2017", "February, 15 2017", "March, 01 2017" ,"March, 15 2017", "April, 03 2017", "April, 17 2017", "May, 02 2017", "May, 02 2017", "May, 15 2017", "May, 15 2017", "June, 01 2017","June, 15 2017" ,"July, 03 2017", "July, 17 2017" ,"August, 01 2017", "August, 01 2017", "August, 15 2017", "August, 15 2017", "September, 01 2017", "September, 15 2017","October, 02 2017","October, 16 2017" ,"November, 01 2017", "November, 01 2017", "November, 15 2017", "November, 15 2017", "December, 01 2017", "December, 15 2017"]

        self.drawScheduleTableView.tableFooterView = UIView()
    } 
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return bond.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("drawCell", forIndexPath: indexPath) as! drawsListTableViewCell
        
        cell.bondLabel.text! = bond[indexPath.row]
        cell.drawLable.text! = draw[indexPath.row]
        cell.dateLabel.text! = date[indexPath.row]
        
        return cell
    }
}
