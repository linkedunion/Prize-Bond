//
//  ForgetPasswordViewController.swift
//  Prize Bond
//
//  Created by Avialdo on 30/12/2015.
//  Copyright © 2015 Avialdo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ForgetPasswordViewController: CustomViewController {

    var arrRes = [String:AnyObject]()
    var status : Int!
    var postingArray = [String:AnyObject]()
    var progressIcon = UIActivityIndicatorView()
    var tempUserId : String!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        progressIcon = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        progressIcon.frame = CGRect(x: self.view.frame.size.width/2 - 25, y: self.view.frame.size.height/2 - 25, width: 50, height: 50)
        progressIcon.backgroundColor = UIColor.lightGrayColor()
        progressIcon.color = UIColor.whiteColor()
        view.addSubview(progressIcon)

        emailTextField.autocorrectionType = UITextAutocorrectionType.No
        submitButton.layer.borderColor = UIColor(red: 0.0/255, green: 186.0/255, blue: 210.0/255, alpha: 1.0).CGColor
        submitButton.layer.borderWidth = 2.0
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    @IBAction func submitButtonAction(sender: AnyObject)
    {
        if(emailTextField.text! == "")
        {
            showAlertBoxWithTitle("Sorry", customMessage: "please fill the email field first")
        }
        else if(!isValidEmail(emailTextField.text!))
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Please provide a valid email address")
        }
        else
        {
            progressIcon.startAnimating()
            let postBodyString = "email=\(emailTextField.text!)"
            
            Alamofire.request(.POST,"http://prizebond.avialdo.com/webservices/forget_password.php", parameters: [:],  encoding: .Custom({
                (convertible, params) in
                let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
                mutableRequest.HTTPBody = postBodyString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:  false)
                return (mutableRequest, nil)
            }), headers: nil).responseJSON { (responseData) -> Void in
                
                if(responseData.2.value != nil)
                {
                    self.progressIcon.stopAnimating()
                    let swiftyJsonVar = JSON(responseData.2.value!)
                    if let resData = swiftyJsonVar[].dictionaryObject
                    {
                        self.arrRes = resData
                    }
                    if self.arrRes.count > 0
                    {
                        print(self.arrRes)
                        self.parseData()
                    }
                }
                else
                {
                    self.progressIcon.stopAnimating()
                    self.showAlertBoxWithTitle("Network Failure", customMessage: "There is some problem with your internet. Please try again later.")
                }
            }
        }
    }
    
    func parseData()
    {
        status = self.arrRes["status"] as! Int
        
        if(status == 1)
        {
            tempUserId = self.arrRes["user_id"] as! String
            performSegueWithIdentifier("changePasswordSegue", sender: self)
        }
        else if(status == -1)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Something went wrong")
        }
        else if(status == -2)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "The email address you provided does not exist")
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if(segue.identifier == "changePasswordSegue")
        {
            let change_password = segue.destinationViewController as! changePasswordViewController
            change_password.tempUserID = tempUserId
        }
    }

}
