//
//  ResultViewController.swift
//  prize_bond
//
//  Created by Avialdo on 29/12/2015.
//  Copyright © 2015 Avialdo. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController , UITableViewDataSource, UITableViewDelegate {

    var denominationValue : String!
    var winLossStatus : String!
    
    @IBOutlet weak var faceImage: UIImageView!
    @IBOutlet weak var winOrLossLabel: UILabel!
    @IBOutlet weak var numberOfEntries: UILabel!
    @IBOutlet weak var resultTableView: UITableView!
    
    var insideArray = [AnyObject]()
    var ResultArray = [String:AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.resultTableView.layer.borderColor = UIColor.grayColor().CGColor
        self.resultTableView.layer.borderWidth = 1.0
        self.resultTableView.tableFooterView = UIView()
        
        if(self.ResultArray["nowin"] == nil)
        {
            // win
            self.numberOfEntries.hidden = false
            
            self.resultTableView.hidden = false
            self.winOrLossLabel.text = "CONGRATULATIONS"
            
            insideArray = ResultArray["win"] as! Array
            self.numberOfEntries.text = "\(insideArray.count) Entries Found"
            self.faceImage.image = UIImage(named: "win")
        }
        else
        {
            // no win
            self.numberOfEntries.hidden = true
            self.resultTableView.hidden = true
            self.winOrLossLabel.text = "No Entry Found"
            self.faceImage.image = UIImage(named: "nowin")
        }

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return insideArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("resultCell", forIndexPath: indexPath) as! ResultTableViewCell
        if(self.ResultArray["nowin"] == nil)
        {
            let bondDetailDictionary = insideArray[indexPath.row]["bond_details"] as! NSDictionary
            cell.bond_number.text = "Bond Number: " + (bondDetailDictionary["bond_number"]! as! String)
            cell.bond_denomination.text = "Denomination Value: " + self.denominationValue
            cell.bond_category.text = "Category: " + (insideArray[indexPath.row]["category"]! as! String)
            cell.bond_amount.text = "Prize: " + (insideArray[indexPath.row]["prize"]! as! String)
            cell.bond_draw_date.text = "Draw Date: " + (insideArray[indexPath.row]["draw_date"]! as! String)
        }

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
