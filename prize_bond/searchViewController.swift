//
//  searchViewController.swift
//  prize_bond
//
//  Created by Avialdo on 18/11/2015.
//  Copyright © 2015 Avialdo. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON

class searchViewController: CustomViewController, UITableViewDataSource,UITableViewDelegate,bondSearchPopOverViewControllerDelegate,UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchDrawText: UITextField!
    @IBOutlet weak var hiddenButton: UIButton!
    @IBOutlet weak var clickButton: UIButton!
    @IBOutlet weak var checkBoxButton: UIButton!
    
    @IBOutlet weak var whenNoBondsButton: UIButton!
    
    @IBAction func checkBoxAction(sender: AnyObject)
    {
        if(checkBoxButton.selected == true)
        {
            checkBoxButton.selected = false
            clickButton.enabled = true
            hiddenButton.enabled = true
            searchDrawText.text = ""
            checkBoxButton.setBackgroundImage(UIImage(named: "unchecked_checkbox"), forState: UIControlState.Normal)
            
        }
        else
        {
            searchDrawText.text = "All Draws"
            searchDrawTextId = "0"
            hiddenButton.enabled = false
            clickButton.enabled = false
            checkBoxButton.setBackgroundImage(UIImage(named: "checked_checkbox"), forState: UIControlState.Normal)
            checkBoxButton.selected = true
        }
    }
    
    @IBAction func whenNoBondAction(sender: AnyObject)
    {
        performSegueWithIdentifier("whenNoBondsSegue", sender: self)
    }
    
    var arrDates = [[String:AnyObject]]()
    var timeToCallWebservice = true
    var denominationValue : String!
    var denominationId : String!
    var arrRes = [String:AnyObject]()
    var udidTime :Bool!
    var searchDrawTextId : String!
    var updatingArray = []
    
    var postingArray = [String:AnyObject]()

    var bondsToAddArray = [AnyObject]()
    var bondsToDeleteArray = [AnyObject]()
    var bondsToLookup = [AnyObject]()
    var postingString : String!
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    var people = [NSManagedObject]()
    var uploadArray = [NSManagedObject]()
    
    @IBOutlet weak var searchTableView: UITableView!
    
    override func viewDidLoad()
    {
        print("the denomination value is" , denominationValue)
        super.viewDidLoad()
        checkBoxButton.selected = false
        searchTableView.layer.borderColor = UIColor.grayColor().CGColor
        searchTableView.layer.borderWidth = 1.0
        searchTableView.tableFooterView = UIView()
        
        if(NSUserDefaults.standardUserDefaults().objectForKey("userID") == nil)
        {
            sendUDIDandGetUserID()
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)

        var predicate = NSPredicate(format: "id == %@ && denominationValue == %@", "0",self.denominationValue)
        self.deleteFromDatabase(predicate)
        
        predicate = NSPredicate(format: "publish == %@ && denominationValue == %@ && id == %@",false, self.denominationValue, "1")
        fetchDataAndDisplayInTableView(predicate)
        searchTableView.reloadData()
        
        activityIndicator.hidden = true
        self.clickButton.enabled = false
        self.hiddenButton.enabled = false
        
        timeToCallWebservice = true
        if(timeToCallWebservice == true && people.count > 0)
        {
            activityIndicator.hidden = false
            activityIndicator.startAnimating()
            let postBodyString = "denomination_id=\(denominationId)"
            
            Alamofire.request(.POST,"http://prizebond.avialdo.com/webservices/get_draw_list.php", parameters: [:], encoding: .Custom({
                (convertible, params) in
                let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
                mutableRequest.HTTPBody = postBodyString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
                return (mutableRequest, nil)
            }), headers: nil).responseJSON { (responseData) -> Void in
                
                if (responseData.2.value != nil)
                {
                    let swiftyJsonVar = JSON(responseData.2.value!)
                    if let resData = swiftyJsonVar[].arrayObject
                    {
                        self.arrDates = resData as! [[String:AnyObject]]
                    }
                    if self.arrDates.count > 0 {
                        self.timeToCallWebservice = false
                        var dict = self.arrDates[0]
                        self.searchDrawText.text = dict["draw_date"] as? String
                        self.clickButton.enabled = true
                        self.hiddenButton.enabled = true
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.hidden = true
                    }
                }
                else
                {
                    self.showAlertBoxWithTitle("Network Failure", customMessage: "There is some problem with your internet. Please try again later.")
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.hidden = true
                }
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        searchDrawText.resignFirstResponder() 
    }
    
    @IBAction func clicked(sender: AnyObject)
    {
        performSegueWithIdentifier("searchPopOverSegue", sender: sender)
    }
    
    func sendUDIDandGetUserID()
    {
        let myUdidValue :String = NSUserDefaults.standardUserDefaults().objectForKey("UDID")! as! String
        let postBodyString = "UDID=\(myUdidValue)"
        
        print(postBodyString)
        
        Alamofire.request(.POST,"http://prizebond.avialdo.com/webservices/register_udid.php", parameters: [:],  encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.HTTPBody = postBodyString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:  false)
            return (mutableRequest, nil)
        }), headers: nil).responseJSON { (responseData) -> Void in
            if(responseData.2.value != nil)
            {
                let swiftyJsonVar = JSON(responseData.2.value!)
                if let resData = swiftyJsonVar[].dictionaryObject {
                    self.arrRes = resData
                }
                if self.arrRes.count > 0
                {
                    NSUserDefaults.standardUserDefaults().setObject(self.arrRes["user_id"]!, forKey: "userID")
                }
            }
            else
            {
                self.showAlertBoxWithTitle("Network Failure", customMessage: "There is some problem with your internet. Please try again later.")
            }
        }
    }
    
    @IBAction func deleteButtonClicked(sender: AnyObject)
    {
        if(people.count > 0)
        {
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want to delete all these bonds?", preferredStyle: UIAlertControllerStyle.Alert)
            
            let yesAddThem = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                for index in 0..<self.people.count
                {
                    let person = self.people[0]
                    
                    print((person.valueForKey("number") as! String))
                    
                    let predicate = NSPredicate(format: "publish == %@ && denominationValue == %@ && number == %@", false,self.denominationValue,(person.valueForKey("number") as! String))
                    self.updateValue(predicate, changingKey: "publish", changingValue: 1)
                    self.people.removeAtIndex(0)
                }
                self.searchTableView.reloadData()
            })
            
            let NoDont = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
            })
            
            alert.addAction(yesAddThem)
            alert.addAction(NoDont)
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func searchClicked(sender: AnyObject)
    {
        if(people.count > 0)
        {
            var predicate = NSPredicate(format: "sync == %@ && denominationValue == %@",false,self.denominationValue)
            CustomFetchForTemporaryUse(predicate)
            
            //making of bonds to add array //
            var bondsToAdd = Dictionary<String,String>()
            
            bondsToAddArray.removeAll()
            
            for i in 0..<uploadArray.count
            {
                let person = uploadArray[i]
                
                bondsToAdd["denomination_id"] = denominationId
                bondsToAdd["number"] = person.valueForKey("number") as? String
                bondsToAddArray.append(bondsToAdd)
            }
            
            predicate = NSPredicate(format: "publish == %@ && denominationValue == %@",true,self.denominationValue)
            CustomFetchForTemporaryUse(predicate)
            
            //making of bonds to add array //
            var bondsToDelete = Dictionary<String,String>()
            bondsToDeleteArray.removeAll()
            
            for i in 0..<uploadArray.count
            {
                let person = uploadArray[i]
                
                bondsToDelete["denomination_id"] = denominationId
                bondsToDelete["number"] = person.valueForKey("number") as? String
                bondsToDeleteArray.append(bondsToDelete)
            }
            
            if(searchDrawText.text != "")
            {
                do
                {
                    print("the denomination id is" , denominationId)
                    postingArray = ["user_id": NSUserDefaults.standardUserDefaults().valueForKey("userID")!, "denomination_id": denominationId, "file_id": searchDrawTextId, "BondsToAdd":bondsToAddArray,"BondsToDelete": bondsToDeleteArray, "lookup": bondsToLookup]
                    
                    let postingJson : NSData = try NSJSONSerialization.dataWithJSONObject(postingArray, options: [])
                    postingString = String.init(data: postingJson, encoding: NSUTF8StringEncoding)!
                }
                catch
                {
                    
                }
                sendAllThreeArrays("searchButton")
                print(postingString)
            }
            else
            {
                showAlertBoxWithTitle("Sorry", customMessage: "Please select a search date first")
            }
        }
        else
        {
            showAlertBoxWithTitle("Sorry", customMessage: "You have no bonds to search")
        }
    }
    
    func sendAllThreeArrays(buttonType : String)
    {
        let stringValue = "bondsArray=\(postingString)"
        Alamofire.request(.POST,"http://prizebond.avialdo.com/webservices/insert_bond_numbers.php", parameters: [:], encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.HTTPBody = stringValue.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:  false)
            return (mutableRequest, nil)
        }), headers: nil).responseJSON { (responseData) -> Void in
            if(responseData.2.value != nil)
            {
                let swiftyJsonVar = JSON(responseData.2.value!)
                if let resData = swiftyJsonVar[].dictionaryObject {
                    self.arrRes = resData
                }
                if self.arrRes.count > 0
                {
                    if(buttonType == "searchButton")
                    {
                        let predicate = NSPredicate(format: "sync == %@ && denominationValue == %@", false,self.denominationValue)
                        self.updateValue(predicate, changingKey: "sync", changingValue: 1)
                        //self.printWholeData()
                        
                        if(self.arrRes["status"]! as! Int == 1)
                        {
                            let predicate = NSPredicate(format: "publish == %@  && denominationValue == %@",true,self.denominationValue)
                            self.deleteFromDatabase(predicate)
                            self.performSegueWithIdentifier("resultScreenSegue2", sender: self)
                        }
                    }
                }
            }
            else
            {
                self.showAlertBoxWithTitle("Network Failure", customMessage: "There is some problem with your internet. Please try again later.")
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if(segue.identifier == "searchPopOverSegue")
        {
            let popOver = segue.destinationViewController as! bondSearchPopOverViewController
            popOver.pDelegate = self
            popOver.denominationId = self.denominationId
            popOver.timeToCallWebservice = self.timeToCallWebservice
            popOver.arrRes = self.arrDates
            popOver.modalPresentationStyle = .Popover
            popOver.preferredContentSize = CGSize(width: 200, height: 300)
            popOver.view.frame = self.view.frame
            let controller = popOver.popoverPresentationController
            if controller != nil {
                controller?.delegate = self
                controller?.permittedArrowDirections = UIPopoverArrowDirection()
                controller?.sourceRect = self.view.frame
                controller?.sourceView = self.view
                controller?.sourceRect.origin = self.view.frame.origin
            }
        }
            
        else if(segue.identifier == "resultScreenSegue2")
        {
            let resultScreen = segue.destinationViewController as! ResultViewController
            resultScreen.ResultArray = self.arrRes
            resultScreen.denominationValue = self.denominationValue
            print(self.arrRes)

//            print(insideArray[0]["bond_details"])
            //print(self.arrRes["win"]["category"][0] as? String)
        }
        
        else if(segue.identifier == "whenNoBondsSegue")
        {
            let prizeBondScreen = segue.destinationViewController as! PrizeBondViewController
            prizeBondScreen.isSearchDisplay = false
            print(self.denominationValue)
            prizeBondScreen.tempTextFieldtext = self.denominationValue
        }
    }
    
    func fetchDataAndDisplayInTableView(customPredicate: NSPredicate)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Numbers")
        
        fetchRequest.predicate = customPredicate
        do
        {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            people = results as! [NSManagedObject]
        }
            catch let error as NSError
            {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func CustomFetchForTemporaryUse(customPredicate: NSPredicate)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Numbers")
        
        fetchRequest.predicate = customPredicate
        do
        {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            uploadArray = results as! [NSManagedObject]
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func updateValue(customPredicate : NSPredicate, changingKey: String, changingValue: Int)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let fetchRequest = NSFetchRequest(entityName: "Numbers")
        
        fetchRequest.predicate = customPredicate
        do
        {
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            updatingArray = results as! [NSManagedObject]
            
            if(updatingArray.count>0)
            {
                for i in 0..<updatingArray.count
                {
                    let person = updatingArray[i]
                    person.setValue(changingValue, forKey: changingKey)
                    do {
                        try person.managedObjectContext?.save()
                    } catch {
                        let saveError = error as NSError
                        print(saveError)
                    }

                }
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func printWholeData()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let fetchRequest = NSFetchRequest(entityName: "Numbers")
        let predicate = NSPredicate(format: "denominationValue == %@",self.denominationValue)
        fetchRequest.predicate = predicate
        do
        {
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            updatingArray = results as! [NSManagedObject]
            
            if(updatingArray.count>0)
            {
                for i in 0..<updatingArray.count
                {
                    let person = updatingArray[i]
                    let syncValue = person.valueForKey("publish") as! Bool
                    print("the publish value is \(syncValue)")
                }
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return people.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if(people.count > 0)
        {
            whenNoBondsButton.hidden = true
        }
        else
        {
            whenNoBondsButton.hidden = false
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("searchCell", forIndexPath: indexPath) as! SearchTableViewCell
        
        let person = people[indexPath.row]
        
        cell.bond_number!.text = person.valueForKey("number") as? String
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if editingStyle == UITableViewCellEditingStyle.Delete
        {
            let person = people[indexPath.row]
            
            print((person.valueForKey("number") as! String))
            
            let predicate = NSPredicate(format: "publish == %@ && denominationValue == %@ && number == %@", false,self.denominationValue,(person.valueForKey("number") as! String))
            self.updateValue(predicate, changingKey: "publish", changingValue: 1)
            people.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    
    func deleteFromDatabase(customPredicate: NSPredicate)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let fetchRequest = NSFetchRequest(entityName: "Numbers")
        
        fetchRequest.predicate = customPredicate
        
        do
        {
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            var deleteableRecords = [NSManagedObject]()
            
            deleteableRecords = results as! [NSManagedObject]
            if deleteableRecords.count > 0 {
                
                for result: AnyObject in deleteableRecords{
                    appDelegate.managedObjectContext.deleteObject(result as! NSManagedObject)
                    print("NSManagedObject has been Deleted")
                }
            }
            try managedObjectContext.save()
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
    }
    
    func popoverControllerDidDismissPopover(popoverController: UIModalPresentationStyle) {
        
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    func selectDate(date: String, drawID: String, array:[[String:AnyObject]]) {
        searchDrawText.text = date
        searchDrawTextId = drawID
        timeToCallWebservice = false
    }
}
