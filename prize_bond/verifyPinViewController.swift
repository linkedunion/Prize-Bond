//
//  verifyPinViewController.swift
//  Prize Bond
//
//  Created by Avialdo on 07/01/2016.
//  Copyright © 2016 Avialdo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class verifyPinViewController: CustomViewController {

    @IBOutlet weak var pintextField: UITextField!
    
    var arrRes = [String:AnyObject]()
    var arrForExistingBonds = [String:AnyObject]()
    var status : Int!
    var postingArray = [String:AnyObject]()
    var progressIcon = UIActivityIndicatorView()
    var tempUserID : String!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let fetchRequest = NSFetchRequest(entityName: "Numbers")
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var people = [NSManagedObject]()
    
    var bondsToDeleteArray = [AnyObject]()
    var uploadArray = [NSManagedObject]()
    var postingString : String!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        progressIcon = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        progressIcon.frame = CGRect(x: self.view.frame.size.width/2 - 25, y: self.view.frame.size.height/2 - 25, width: 50, height: 50)
        progressIcon.backgroundColor = UIColor.lightGrayColor()
        progressIcon.color = UIColor.whiteColor()
        view.addSubview(progressIcon)
        
        pintextField.autocorrectionType = UITextAutocorrectionType.No
    }
    
    func parseData()
    {
        status = self.arrRes["status"] as! Int
        
        if(status == 1)
        {                               //// call web service ////
            if(tempUserID != "0")
            {
                arrForExistingBonds = self.arrRes
                self.getBondsCount()
            }
            else
            {
                NSUserDefaults.standardUserDefaults().setValue("loggedIn", forKey: "loggedIn")
                performSegueWithIdentifier("pinVerifiedSegue", sender: self)
            }
        }
        else if(status == -1)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Something went wrong")
        }
        else if(status == -2)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Wrong token provided")
        }
    }
    
    func parseDataForSync()
    {
        status = self.arrRes["status"] as! Int
        
        if(status == -1)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Something went wrong. Please try again later")
        }
    }
    
    override func keyboardWillShow(notification: NSNotification)
    {
        if(self.view.frame.origin.y == screen)
        {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()
            {
                self.view.frame.origin.y -= (keyboardSize.height - 30)
            }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidChange(textField: UITextField)
    {
        if(textField.text?.characters.count > 5)
        {
            let postBodyString = "token=\(pintextField.text!)&user_id=" + (NSUserDefaults.standardUserDefaults().objectForKey("userID")! as! String)
            self.webServiceMethodWithPostBody(postBodyString, url: "http://prizebond.avialdo.com/webservices/activate_user.php", parseMethod: "verifyPin")
        }
    }

    
    func webServiceMethodWithPostBody(postBodyString: String,url: String, parseMethod: String)
    {
        progressIcon.startAnimating()
        
        Alamofire.request(.POST,url, parameters: [:],  encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.HTTPBody = postBodyString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:  false)
            return (mutableRequest, nil)
        }), headers: nil).responseJSON { (responseData) -> Void in
            if(responseData.2.value != nil)
            {
                self.progressIcon.stopAnimating()
                let swiftyJsonVar = JSON(responseData.2.value!)
                if let resData = swiftyJsonVar[].dictionaryObject
                {
                    self.arrRes = resData
                }
                if self.arrRes.count > 0
                {
                    print(self.arrRes)
                    if(parseMethod == "verifyPin")
                    {
                        self.parseData()
                    }
                    else if(parseMethod == "updateSync")
                    {
                        self.parseDataForSync()
                    }
                }
            }
            else
            {
                self.progressIcon.stopAnimating()
                self.showAlertBoxWithTitle("Network Failure", customMessage: "There is some problem with your internet. Please try again later.")
            }
        }
    }
    
    func getBondsCount()
    {
        do
        {
            let fetchRequest = NSFetchRequest(entityName: "Numbers")
            
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            var bonds = [NSManagedObject]()
            
            bonds = results as! [NSManagedObject]
            print("the bonds count is", bonds.count)
            if (bonds.count > 0)
            {
                let alert = UIAlertController(title: "Alert", message: "It seems you have some bonds already. Do you want to save them?", preferredStyle: UIAlertControllerStyle.Alert)
                
                let yesAddThem = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    // sync webservice
                    
                    let postBodyString = "guest_id=\(self.tempUserID)&user_id=" + (NSUserDefaults.standardUserDefaults().objectForKey("userID")! as! String)
                    self.webServiceMethodWithPostBody(postBodyString, url: "http://prizebond.avialdo.com/webservices/update_guest_user_id.php", parseMethod: "updateSync")
                    
                    self.existingBondsAddingWork()
                    
                })
                
                let NoDont = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    // local delete and no sync web service
                    self.bondsToDeleteArrayMethod()
                    let predicate = NSPredicate(format: "publish == %@",true)
                    self.deleteAllFromDatabase(predicate, allDelete: true)
                    self.existingBondsAddingWork()
                })
                
                alert.addAction(yesAddThem)
                alert.addAction(NoDont)
                
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                self.existingBondsAddingWork()
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }

    func existingBondsAddingWork()
    {
        let existing = self.arrForExistingBonds["existingBonds"] as! NSArray
        
        if(existing.count > 0)
        {
            for index in 0..<existing.count
            {
                addBond(existing[index]["denomination_value"] as! String, demoninationId: Int(existing[index]["denomination_id"] as! String)! , id: 1, name: existing[index]["bond_number"] as! String, publish: 0, Sync: 1)
                print(existing[index]["bond_number"] as! String)
            }
        }
        NSUserDefaults.standardUserDefaults().setValue("loggedIn", forKey: "loggedIn")
        performSegueWithIdentifier("pinVerifiedSegue", sender: self)
    }

    func addBond(denominationValue: String, demoninationId: NSNumber, id: NSNumber,name: String, publish: NSNumber, Sync: NSNumber)
    {
        let entity =  NSEntityDescription.entityForName("Numbers", inManagedObjectContext:managedObjectContext)
        let person = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext: managedObjectContext)
        
        print(denominationValue ,  demoninationId , id , name , publish , Sync)
        
        person.setValue(denominationValue, forKey: "denominationValue")
        person.setValue(demoninationId, forKey: "denominationId")
        person.setValue(id, forKey: "id")
        person.setValue(name, forKey: "number")
        person.setValue(publish, forKey: "publish")
        person.setValue(Sync, forKey: "sync")
        
        people.append(person)
        
        do
        {
            print("bond saved")
            try managedObjectContext.save()
            people.removeAll()
        }
            
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }

    func deleteAllFromDatabase(customPredicate : NSPredicate, allDelete:Bool)
    {
        do
        {
            print(allDelete)
            let fetchRequest = NSFetchRequest(entityName: "Numbers")
            
            if(allDelete == true)
            {
            }
            else
            {
                fetchRequest.predicate = customPredicate
            }
            
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            var deleteableRecords = [NSManagedObject]()
            
            deleteableRecords = results as! [NSManagedObject]
            if deleteableRecords.count > 0 {
                
                for result: AnyObject in deleteableRecords{
                    appDelegate.managedObjectContext.deleteObject(result as! NSManagedObject)
                    print("NSManagedObject has been Deleted")
                }
            }
            try managedObjectContext.save()
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }

    func bondsToDeleteArrayMethod()
    {
        //var predicate = NSPredicate(format: "publish == %@ && denominationValue == %@",true,self.denominationValue)
        CustomFetchForTemporaryUse()
        
        //making of bonds to add array //
        var bondsToDelete = Dictionary<String,AnyObject>()
        bondsToDeleteArray.removeAll()
        
        for i in 0..<uploadArray.count
        {
            let person = uploadArray[i]
            
            print(person.valueForKey("denominationId")!)
            
            bondsToDelete["denomination_id"] = person.valueForKey("denominationId")!
            bondsToDelete["number"] = person.valueForKey("number") as? String
            bondsToDeleteArray.append(bondsToDelete)
        }
        do
        {
            postingArray = ["user_id": self.tempUserID, "denomination_id": "1", "file_id": "1", "BondsToAdd": [], "BondsToDelete": bondsToDeleteArray, "lookup": []]
            
            let postingJson : NSData = try NSJSONSerialization.dataWithJSONObject(postingArray, options: [])
            postingString = String.init(data: postingJson, encoding: NSUTF8StringEncoding)!
            
            print("the posting string will be ", postingString)
        }
        catch
        {
            
        }
        
        let stringValue = "bondsArray=\(postingString)"
        
        self.webServiceMethodWithPostBody(stringValue, url: "http://prizebond.avialdo.com/webservices/insert_bond_numbers.php", parseMethod: "doNothing")
        print(postingString)
    }
    
    func CustomFetchForTemporaryUse()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Numbers")
        
        do
        {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            uploadArray = results as! [NSManagedObject]
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
//    func sendAllThreeArrays()
//    {
//        let stringValue = "bondsArray=\(postingString)"
//        Alamofire.request(.POST,"http://prizebond.avialdo.com/webservices/insert_bond_numbers.php", parameters: [:], encoding: .Custom({
//            (convertible, params) in
//            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
//            mutableRequest.HTTPBody = stringValue.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:  false)
//            return (mutableRequest, nil)
//        }), headers: nil).responseJSON { (responseData) -> Void in
//            if(responseData.2.value != nil)
//            {
//                let swiftyJsonVar = JSON(responseData.2.value!)
//                if let resData = swiftyJsonVar[].dictionaryObject {
//                    self.arrRes = resData
//                }
//                if self.arrRes.count > 0
//                {
//                    //                    self.existingBondsAddingWork()
//                    //                    self.updateValue("sync", changingValue: 0)
//                    print("the result is" , self.arrRes)
//                }
//            }
//        }
//    }
}
