//
//  RegisterViewController.swift
//  Prize Bond
//
//  Created by Avialdo on 30/12/2015.
//  Copyright © 2015 Avialdo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class RegisterViewController: CustomViewController {

    var arrRes = [String:AnyObject]()
    var status : Int!
    var postingArray = [String:AnyObject]()
    var progressIcon = UIActivityIndicatorView()
    
    @IBOutlet weak var sign_up_button: UIButton!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var verifyPassword: UITextField!
    @IBOutlet weak var checkBox: UIButton!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let fetchRequest = NSFetchRequest(entityName: "Numbers")
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        progressIcon = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        progressIcon.frame = CGRect(x: self.view.frame.size.width/2 - 25, y: self.view.frame.size.height/2 - 25, width: 50, height: 50)
        progressIcon.backgroundColor = UIColor.lightGrayColor()
        progressIcon.color = UIColor.whiteColor()
        view.addSubview(progressIcon)
        
        sign_up_button.layer.borderColor = UIColor(red: 0.0/255, green: 186.0/255, blue: 210.0/255, alpha: 1.0).CGColor
        sign_up_button.layer.borderWidth = 2.0
        
        email.autocorrectionType = UITextAutocorrectionType.No
        password.autocorrectionType = UITextAutocorrectionType.No
        verifyPassword.autocorrectionType = UITextAutocorrectionType.No

    }
    
    @IBAction func signInAction(sender: AnyObject)
    {
        navigationController!.popViewControllerAnimated(true)
    }
    
    @IBAction func checkBoxAction(sender: AnyObject)
    {
        if(checkBox.selected)
        {
            checkBox.selected = false
            checkBox.setBackgroundImage(UIImage(named: "unchecked_checkbox"), forState: UIControlState.Normal)
        }
        else
        {
            checkBox.setBackgroundImage(UIImage(named: "checked_checkbox"), forState: UIControlState.Normal)
            checkBox.selected = true
        }
    }
    
    @IBAction func sign_up_action(sender: AnyObject)
    {
        if(email.text! == "" || password.text! == "" || verifyPassword.text! == "")
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Please fill all the fields first")
        }
        else if(!isValidEmail(email.text!))
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Please provide a valid email address")
        }
        else if(password.text?.characters.count < 4)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Your password must be of atleast 4 characters")
        }
        else if(password.text != verifyPassword.text)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Your password and verify password do not match")
        }
        else if(!checkBox.selected)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Please agree to the terms and conditions.")
        }
        else
        {
            progressIcon.startAnimating()
            
            let myUdidValue :String = NSUserDefaults.standardUserDefaults().objectForKey("UDID")! as! String
            var postBodyString : String!
            
            if(NSUserDefaults.standardUserDefaults().objectForKey("userID") != nil)
            {
                print("condition1")
                postBodyString = "UDID=\(myUdidValue)&email=\(email.text!)&password=\(password.text!)&user_id=" + (NSUserDefaults.standardUserDefaults().objectForKey("userID")! as! String)
            }
            else
            {
                print("condition2")
                postBodyString = "UDID=\(myUdidValue)&email=\(email.text!)&password=\(password.text!)&user_id=" + ""
            }
            
            
            //print(postBodyString)
            
            Alamofire.request(.POST,"http://prizebond.avialdo.com/webservices/register_udid_sync.php", parameters: [:],  encoding: .Custom({
                (convertible, params) in
                let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
                mutableRequest.HTTPBody = postBodyString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:  false)
                return (mutableRequest, nil)
            }), headers: nil).responseJSON { (responseData) -> Void in
                if(responseData.2.value != nil)
                {
                    self.progressIcon.stopAnimating()
                    let swiftyJsonVar = JSON(responseData.2.value!)
                    if let resData = swiftyJsonVar[].dictionaryObject
                    {
                        self.arrRes = resData
                    }
                    if self.arrRes.count > 0
                    {
                        print(self.arrRes)
                        self.parseData()
                    }
                }
                else
                {
                    self.progressIcon.stopAnimating()
                    self.showAlertBoxWithTitle("Network Failure", customMessage: "There is some problem with your internet. Please try again later.")
                }
            }
        }
    }
    
    func parseData()
    {
        status = self.arrRes["status"] as! Int
        
        if(status == 1)
        {
            print(self.arrRes["user_id"]!)
            
            NSUserDefaults.standardUserDefaults().setValue(self.arrRes["user_id"]!, forKey: "userID")
            performSegueWithIdentifier("verifyPinSegue", sender: self)
        }
        else if(status == -2)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "The email address already exist")
        }
        else if(status == -1 || status == -3 || status == -4)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Something went wrong")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func keyboardWillShow(notification: NSNotification)
    {
        if(self.view.frame.origin.y == screen)
        {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()
            {
                self.view.frame.origin.y -= (keyboardSize.height - 30)
            }
        }
    }
    
//    func getBondsCount() -> Bool
//    {
//        do
//        {
//            let results =
//            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
//            var bonds = [NSManagedObject]()
//            bonds = results as! [NSManagedObject]
//            
//            if(bonds.count > 0)
//            {
//                return true
//            }
//        }
//        catch let error as NSError
//        {
//            print("Could not fetch \(error), \(error.userInfo)")
//        }
//        
//        return false
//    }
//   
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if(segue.identifier == "verifyPinSegue")
        {
            let verify_pin = segue.destinationViewController as! verifyPinViewController
            verify_pin.tempUserID = "0"
        }
    }

}
