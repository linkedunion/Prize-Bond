//
//  Login.swift
//  Prize Bond
//
//  Created by Avialdo on 30/12/2015.
//  Copyright © 2015 Avialdo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class Login: CustomViewController {

    var arrRes = [String:AnyObject]()
    var arrForExistingBonds = [String:AnyObject]()
    var status : Int!
    var tempUserID : String!
    
    var progressIcon = UIActivityIndicatorView()
    var temp_user_id : String!
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let fetchRequest = NSFetchRequest(entityName: "Numbers")
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var people = [NSManagedObject]()

    @IBOutlet weak var sign_in_button: UIButton!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    var postingArray = [String:AnyObject]()
    var bondsToDeleteArray = [AnyObject]()
    var uploadArray = [NSManagedObject]()
    var postingString : String!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        emailTextfield.autocorrectionType = UITextAutocorrectionType.No
        passwordTextfield.autocorrectionType = UITextAutocorrectionType.No
        
        sign_in_button.layer.borderColor = UIColor(red: 0.0/255, green: 186.0/255, blue: 210.0/255, alpha: 1.0).CGColor
        sign_in_button.layer.borderWidth = 2.0
        
        progressIcon = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        progressIcon.frame = CGRect(x: self.view.frame.size.width/2 - 25, y: self.view.frame.size.height/2 - 25, width: 50, height: 50)
        progressIcon.backgroundColor = UIColor.lightGrayColor()
        progressIcon.color = UIColor.whiteColor()
        view.addSubview(progressIcon)
    }
    
    @IBAction func signInAction(sender: AnyObject)
    {
        if(emailTextfield.text! == "" || passwordTextfield.text! == "")
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Please fill all the fields first")
        }
        else if(!isValidEmail(emailTextfield.text!))
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Please provide a valid email address")
        }
        else
        {
            let postBodyString = "email=\(emailTextfield.text!)&password=\(passwordTextfield.text!)"
            
            self.webServiceMethodWithPostBody(postBodyString, url: "http://prizebond.avialdo.com/webservices/login.php", parseMethod: "parse_login")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func showAlertBoxForVerifyScreenWithTitle(customTitle:String, customMessage:String)
    {
        let alert = UIAlertController(title: customTitle, message: customMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
        let VerifyTime = UIAlertAction(title: "Yes", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let postBodyString = "email=\(self.emailTextfield.text!)&user_id=\(self.temp_user_id)"
            self.webServiceMethodWithPostBody(postBodyString, url: "http://prizebond.avialdo.com/webservices/regenerate_activate_token.php", parseMethod: "parse_regenerate")
            
            })
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil))
        alert.addAction(VerifyTime)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
                                                    ////////////////  PARSING METHODS  //////////////////////
    func parse_login()
    {
        status = self.arrRes["status"] as! Int
        
        if(status == 1)
        {
            // Correct Credentails and the account is active too.
            
            temp_user_id = self.arrRes["user_id"] as! String
            
            arrForExistingBonds = self.arrRes
            
            let predicate = NSPredicate(format: "publish == %@",true)
            deleteAllFromDatabase(predicate, allDelete: false)
            getBondsCount()
        }
        else if(status == -1)
            
        {
            // Something Went Wrong.
            
            showAlertBoxWithTitle("Sorry", customMessage: "Something went wrong")
        }
        else if(status == -2)
        {
            // Correct Credentials but the account is not active.
            
            if(self.getBondsCountInTotal())
            {
                self.tempUserID = NSUserDefaults.standardUserDefaults().valueForKey("userID")! as! String
                print("the temp user id is " , self.tempUserID)
            }
            showAlertBoxForVerifyScreenWithTitle("Sorry", customMessage: "This account is not activated yet. Do you want to activate this account now?")
            temp_user_id = self.arrRes["user_id"] as! String
        }
        else if(status == -3)
        {
            // Incorrect Crdentials
            
            showAlertBoxWithTitle("Sorry", customMessage: "You have provided incorrect credentials.")
        }
    }
    
    func parse_regenerate()
    {
        self.status = self.arrRes["status"] as! Int
        if(self.status == 1)
        {
            NSUserDefaults.standardUserDefaults().setObject(self.temp_user_id, forKey: "userID")
            self.performSegueWithIdentifier("directVerify", sender: self)
        }
    }
    
    func parse_sync()
    {
        status = self.arrRes["status"] as! Int
        
        if(status == 1)
        {
            print("the status is",status)
        }
        else if(status == -1)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Something went wrong. Please try again later")
        }
    }

    
    override func keyboardWillShow(notification: NSNotification)
    {
        if(self.view.frame.origin.y == screen)
        {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()
            {
                self.view.frame.origin.y -= (keyboardSize.height - 30)
            }
        }
    }
    
    func addBond(denominationValue: String, demoninationId: NSNumber, id: NSNumber,name: String, publish: NSNumber, Sync: NSNumber)
    {
        let entity =  NSEntityDescription.entityForName("Numbers", inManagedObjectContext:managedObjectContext)
        let person = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext: managedObjectContext)
        
        print(denominationValue ,  demoninationId , id , name , publish , Sync)
        
        person.setValue(denominationValue, forKey: "denominationValue")
        person.setValue(demoninationId, forKey: "denominationId")
        person.setValue(id, forKey: "id")
        person.setValue(name, forKey: "number")
        person.setValue(publish, forKey: "publish")
        person.setValue(Sync, forKey: "sync")
        
        people.append(person)
        
        do
        {
            print("bond saved")
            try managedObjectContext.save()
            people.removeAll()
        }
            
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }

    func printWholeData()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let fetchRequest = NSFetchRequest(entityName: "Numbers")

        do
        {
            var bonds = [NSManagedObject]()
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            bonds = results as! [NSManagedObject]
            
            if(bonds.count>0)
            {
                for i in 0..<bonds.count
                {
                    let person = bonds[i]
                    let syncValue = person.valueForKey("publish") as! Bool
                    print("the publish value is \(syncValue)")
                }
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func getBondsCount()
    {
        do
        {
            let fetchRequest = NSFetchRequest(entityName: "Numbers")

            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            var bonds = [NSManagedObject]()
            
            bonds = results as! [NSManagedObject]
            print("the bonds count is", bonds.count)
            if (bonds.count > 0)
            {
                self.tempUserID = NSUserDefaults.standardUserDefaults().valueForKey("userID")! as! String  // for guest_id //
                
                let alert = UIAlertController(title: "Alert", message: "It seems you have some bonds already. Do you want to save them?", preferredStyle: UIAlertControllerStyle.Alert)
                
                let yesAddThem = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {
                    (alert: UIAlertAction!) -> Void in
                   
                    print("the guest id is " , self.tempUserID)
                    print("the default id is " , self.temp_user_id)
                    
                    let postBodyString = "guest_id=\(self.tempUserID)&user_id=\(self.temp_user_id)"
                    self.webServiceMethodWithPostBody(postBodyString, url: "http://prizebond.avialdo.com/webservices/update_guest_user_id.php", parseMethod: "updateSync")
                    
                    self.existingBondsAddingWork()
                    NSUserDefaults.standardUserDefaults().setValue("loggedIn", forKey: "loggedIn")
                    
                    //self.bondsToDeleteArrayMethod()
                    
                    })
                    
                let NoDont = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    self.bondsToDeleteArrayMethod()
                    let predicate = NSPredicate(format: "publish == %@",true)
                    self.deleteAllFromDatabase(predicate, allDelete: true)
                    self.existingBondsAddingWork()
                    
                })

                alert.addAction(yesAddThem)
                alert.addAction(NoDont)
                
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                self.existingBondsAddingWork()
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }

    func bondsToDeleteArrayMethod()
    {
        //var predicate = NSPredicate(format: "publish == %@ && denominationValue == %@",true,self.denominationValue)
        CustomFetchForTemporaryUse()
        
        //making of bonds to add array //
        var bondsToDelete = Dictionary<String,AnyObject>()
        bondsToDeleteArray.removeAll()
        
        for i in 0..<uploadArray.count
        {
            let person = uploadArray[i]

            print(person.valueForKey("denominationId")!)

            bondsToDelete["denomination_id"] = person.valueForKey("denominationId")!
            bondsToDelete["number"] = person.valueForKey("number") as? String
            bondsToDeleteArray.append(bondsToDelete)
        }
        
        do
        {
           
            postingArray = ["user_id": NSUserDefaults.standardUserDefaults().valueForKey("userID")!, "denomination_id": "1", "file_id": "1", "BondsToAdd": [], "BondsToDelete": bondsToDeleteArray, "lookup": []]
            
            let postingJson : NSData = try NSJSONSerialization.dataWithJSONObject(postingArray, options: [])
            postingString = String.init(data: postingJson, encoding: NSUTF8StringEncoding)!
            
             print("the posting string will be ", postingString)
        }
        catch
        {
            
        }
        sendAllThreeArrays()
        print(postingString)
    }
    
    func CustomFetchForTemporaryUse()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Numbers")
        
        do
        {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            uploadArray = results as! [NSManagedObject]
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func sendAllThreeArrays()
    {
        let stringValue = "bondsArray=\(postingString)"
        Alamofire.request(.POST,"http://prizebond.avialdo.com/webservices/insert_bond_numbers.php", parameters: [:], encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.HTTPBody = stringValue.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:  false)
            return (mutableRequest, nil)
        }), headers: nil).responseJSON { (responseData) -> Void in
            if(responseData.2.value != nil)
            {
                let swiftyJsonVar = JSON(responseData.2.value!)
                if let resData = swiftyJsonVar[].dictionaryObject {
                    self.arrRes = resData
                }
                if self.arrRes.count > 0
                {
//                    self.existingBondsAddingWork()
//                    self.updateValue("sync", changingValue: 0)
                    print("the result is" , self.arrRes)
                }
            }
            else
            {
                self.progressIcon.stopAnimating()
                self.showAlertBoxWithTitle("Network Failure", customMessage: "There is some problem with your internet. Please try again later.")
            }
        }
    }
    
    func updateValue(changingKey: String, changingValue: Int)
    {
        let fetchRequest = NSFetchRequest(entityName: "Numbers")
        do
        {
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            uploadArray = results as! [NSManagedObject]
            
            if(uploadArray.count>0)
            {
                for i in 0..<uploadArray.count
                {
                    let person = uploadArray[i]
                    person.setValue(changingValue, forKey: changingKey)
                    do {
                        try person.managedObjectContext?.save()
                    } catch {
                        let saveError = error as NSError
                        print(saveError)
                    }
                }
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    
    func existingBondsAddingWork()
    {
        NSUserDefaults.standardUserDefaults().setValue("loggedIn", forKey: "loggedIn")
        NSUserDefaults.standardUserDefaults().setObject(temp_user_id, forKey: "userID")
        let existing = self.arrForExistingBonds["existingBonds"] as! NSArray
        
        if(existing.count > 0)
        {
            for index in 0..<existing.count
            {
                addBond(existing[index]["denomination_value"] as! String, demoninationId: Int(existing[index]["denomination_id"] as! String)! , id: 1, name: existing[index]["bond_number"] as! String, publish: 0, Sync: 1)
                print(existing[index]["bond_number"] as! String)
            }
        }
        performSegueWithIdentifier("skipSegue", sender: self)
    }

    
    func deleteAllFromDatabase(customPredicate : NSPredicate, allDelete:Bool)
    {
        do
        {
            print(allDelete)
            let fetchRequest = NSFetchRequest(entityName: "Numbers")
            
            if(allDelete == true)
            {
            }
            else
            {
                fetchRequest.predicate = customPredicate
            }
            
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            var deleteableRecords = [NSManagedObject]()
            
            deleteableRecords = results as! [NSManagedObject]
            if deleteableRecords.count > 0 {
                
                for result: AnyObject in deleteableRecords{
                    appDelegate.managedObjectContext.deleteObject(result as! NSManagedObject)
                    print("NSManagedObject has been Deleted")
                }
            }
            try managedObjectContext.save()
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if(segue.identifier == "directVerify")
        {
            let verify_pin = segue.destinationViewController as! verifyPinViewController
            print("the temp user id in register screen is ", self.tempUserID)
            verify_pin.tempUserID = self.tempUserID
        }
    }
    
    func getBondsCountInTotal() -> Bool
    {
        do
        {
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            var bonds = [NSManagedObject]()
            bonds = results as! [NSManagedObject]
            
            if(bonds.count > 0)
            {
                return true
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return false
    }
    
    
    func webServiceMethodWithPostBody(postBodyString: String,url: String, parseMethod: String)
    {
        progressIcon.startAnimating()
        
        Alamofire.request(.POST,url, parameters: [:],  encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.HTTPBody = postBodyString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:  false)
            return (mutableRequest, nil)
        }), headers: nil).responseJSON { (responseData) -> Void in
            if(responseData.2.value != nil)
            {
                self.progressIcon.stopAnimating()
                let swiftyJsonVar = JSON(responseData.2.value!)
                if let resData = swiftyJsonVar[].dictionaryObject
                {
                    self.arrRes = resData
                }
                if self.arrRes.count > 0
                {
                    print(self.arrRes)
                    if(parseMethod == "parse_regenerate")
                    {
                        self.parse_regenerate()
                    }
                    else if(parseMethod == "parse_login")
                    {
                        self.parse_login()
                    }
                    else if(parseMethod == "updateSync")
                    {
                        self.parse_sync()
                    }
                }
            }
            else
            {
                self.progressIcon.stopAnimating()
                self.showAlertBoxWithTitle("Network Failure", customMessage: "There is some problem with your internet. Please try again later.")
            }
        }
    }
}
