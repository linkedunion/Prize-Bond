//
//  drawsListCollectionViewCell.swift
//  Prize Bond
//
//  Created by Avialdo on 07/01/2016.
//  Copyright © 2016 Avialdo. All rights reserved.
//

import UIKit

class drawsListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bondLabel: UILabel!

    @IBOutlet weak var drawLable: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
}
