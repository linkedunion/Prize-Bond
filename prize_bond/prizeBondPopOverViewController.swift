//
//  prizeBondPopOverViewController.swift
//  prize_bond
//
//  Created by Avialdo on 19/11/2015.
//  Copyright © 2015 Avialdo. All rights reserved.
//

import UIKit

class prizeBondPopOverViewController: UIViewController,UITableViewDataSource , UITableViewDelegate {

    var pDelegate :prizeBondPopOverViewControllerDelegate?
    
    var denominationArray : [[String]] = [["1","Rs. 100"],["2","Rs. 200"],["3","Rs. 750"],["4","Rs. 1500"],["5","Rs. 7500"],["6","Rs. 15000"],["7","Rs. 25000"],["8","Rs. 40000"]]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return denominationArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("denominationCell", forIndexPath: indexPath) 
        cell.textLabel?.text = denominationArray[indexPath.row][1]
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        pDelegate?.selectBondNumber(Int(denominationArray[indexPath.row][0])!)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

protocol prizeBondPopOverViewControllerDelegate
{
    func selectBondNumber(bondNumber: Int)
}

