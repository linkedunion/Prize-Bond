//
//  ResultTableViewCell.swift
//  prize_bond
//
//  Created by Avialdo on 29/12/2015.
//  Copyright © 2015 Avialdo. All rights reserved.
//

import UIKit

class ResultTableViewCell: UITableViewCell {

    @IBOutlet weak var bond_number: UILabel!
    @IBOutlet weak var bond_denomination: UILabel!
    @IBOutlet weak var bond_category: UILabel!
    @IBOutlet weak var bond_amount: UILabel!
    @IBOutlet weak var bond_draw_date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
