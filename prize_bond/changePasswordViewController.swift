//
//  changePasswordViewController.swift
//  prize_bond
//
//  Created by Avialdo on 07/01/2016.
//  Copyright © 2016 Avialdo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class changePasswordViewController: CustomViewController {

    @IBOutlet weak var pinTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var submitButton: UIButton!
    
    var tempUserID : String!
    var arrRes = [String:AnyObject]()
    var arrForExistingBonds = [String:AnyObject]()
    var status : Int!
    var postingArray = [String:AnyObject]()
    var progressIcon = UIActivityIndicatorView()
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let fetchRequest = NSFetchRequest(entityName: "Numbers")
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var people = [NSManagedObject]()

    var bondsToDeleteArray = [AnyObject]()
    var uploadArray = [NSManagedObject]()
    var postingString : String!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        enableAndDisableTextFields(true)
        
        progressIcon = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        progressIcon.frame = CGRect(x: self.view.frame.size.width/2 - 25, y: self.view.frame.size.height/2 - 25, width: 50, height: 50)
        progressIcon.backgroundColor = UIColor.lightGrayColor()
        progressIcon.color = UIColor.whiteColor()
        view.addSubview(progressIcon)
        
        pinTextField.autocorrectionType = UITextAutocorrectionType.No
        emailTextField.autocorrectionType = UITextAutocorrectionType.No
        passwordTextField.autocorrectionType = UITextAutocorrectionType.No
        
        submitButton.layer.borderColor = UIColor(red: 0.0/255, green: 186.0/255, blue: 210.0/255, alpha: 1.0).CGColor
        submitButton.layer.borderWidth = 2.0
    }

    @IBAction func submitButtonAction(sender: AnyObject)
    {
        if(emailTextField.text! == "" || passwordTextField.text! == "")
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Please fill all the fields first")
        }
        else if(emailTextField.text?.characters.count < 4)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Your password must be of atleast 4 characters")
        }
        else if(emailTextField.text != passwordTextField.text)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Your password and verify password do not match")
        }
        else
        {
            let postBodyString = "user_id=\(tempUserID)&password=\(emailTextField.text!)"
            
            self.webServiceMethodWithPostBody(postBodyString, url: "http://prizebond.avialdo.com/webservices/reset_password.php",parseMethod:"resetPassword")
        }
    }

    func parseDataResetPassword()
    {
        status = self.arrRes["status"] as! Int
        
        if(status == 1)
        {                                               //// call web service //
            arrForExistingBonds = self.arrRes
            
            self.getBondsCount()
            
        }
        else if(status == -1)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Something went wrong")
        }
    }
    
    func parseDataUpdateSync()
    {
        status = self.arrRes["status"] as! Int
        
        if(status == 1)
        {
        }
        else if(status == -1)
        {
            showAlertBoxWithTitle("Sorry", customMessage: "Something went wrong")
        }
    }
    
    func parseData()
    {
        status = self.arrRes["status"] as! Int
        
        if(status == 1)
        {
            self.enableAndDisableTextFields(false)
            UIView.animateWithDuration(1.0, delay: 0,
                options: [.CurveEaseInOut], animations: {
                    self.emailTextField.center.x += self.view.bounds.width
                    self.passwordTextField.center.x -= self.view.bounds.width
                    self.submitButton.center.y -= self.view.bounds.height
                }, completion: nil)
            
            self.statusImageView.image = UIImage(named: "correct")
            
            self.pinTextField.enabled = false
        }
        else if(status == -1)
        {
            self.statusImageView.image = UIImage(named: "wrong")
            showAlertBoxWithTitle("Sorry", customMessage: "Something went wrong")
            self.enableAndDisableTextFields(true)
        }
        else if(status == -2)
        {
            self.statusImageView.image = UIImage(named: "wrong")
            showAlertBoxWithTitle("Sorry", customMessage: "Wrong token provided")
            self.enableAndDisableTextFields(true)
        }
    }
    
    override func keyboardWillShow(notification: NSNotification)
    {
        if(self.view.frame.origin.y == screen)
        {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()
            {
                self.view.frame.origin.y -= (keyboardSize.height - 75)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidChange(textField: UITextField) {
        if(textField.text?.characters.count > 5)
        {
            let postBodyString = "pin=\(pinTextField.text!)&user_id=" + tempUserID
            self.webServiceMethodWithPostBody(postBodyString, url: "http://prizebond.avialdo.com/webservices/verify_pin.php", parseMethod: "verifyPin")
        }
    }
    
    func enableAndDisableTextFields(boolean:Bool)
    {
        self.emailTextField.hidden = boolean
        self.passwordTextField.hidden = boolean
        self.submitButton.hidden = boolean
    }
    
    
    func webServiceMethodWithPostBody(postBodyString: String,url: String, parseMethod: String)
    {
        progressIcon.startAnimating()
        
        Alamofire.request(.POST,url, parameters: [:],  encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.HTTPBody = postBodyString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:  false)
            return (mutableRequest, nil)
        }), headers: nil).responseJSON { (responseData) -> Void in
            if(responseData.2.value != nil)
            {
                self.progressIcon.stopAnimating()
                let swiftyJsonVar = JSON(responseData.2.value!)
                if let resData = swiftyJsonVar[].dictionaryObject
                {
                    self.arrRes = resData
                }
                if self.arrRes.count > 0
                {
                    print(self.arrRes)
                    if(parseMethod == "resetPassword")
                    {
                        self.parseDataResetPassword()
                    }
                    else if(parseMethod == "verifyPin")
                    {
                        self.parseData()
                    }
                    else if(parseMethod == "updateSync")
                    {
                        self.parseDataUpdateSync()
                    }
                }
            }
            else
            {
                self.progressIcon.stopAnimating()
                self.showAlertBoxWithTitle("Network Failure", customMessage: "There is some problem with your internet. Please try again later.")
            }
        }
    }
    
    func getBondsCount()
    {
        do
        {
            let fetchRequest = NSFetchRequest(entityName: "Numbers")
            
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            var bonds = [NSManagedObject]()
            
            bonds = results as! [NSManagedObject]
            print("the bonds count is", bonds.count)
            if (bonds.count > 0)
            {
                let alert = UIAlertController(title: "Alert", message: "It seems you have some bonds already. Do you want to save them?", preferredStyle: UIAlertControllerStyle.Alert)
                
                let yesAddThem = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    // sync webservice
                    
                    print("the user id is " , self.tempUserID)
                    print("the guest id is " , NSUserDefaults.standardUserDefaults().objectForKey("userID")! as! String)
                    
                    let postBodyString = "user_id=\(self.tempUserID)&guest_id=" + (NSUserDefaults.standardUserDefaults().objectForKey("userID")! as! String)
                    self.webServiceMethodWithPostBody(postBodyString, url: "http://prizebond.avialdo.com/webservices/update_guest_user_id.php", parseMethod: "updateSync")
                    
                    self.existingBondsAddingWork()
                    
                })
                
                let NoDont = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    // local delete and server delete //
                    
                    self.bondsToDeleteArrayMethod()
                    let predicate = NSPredicate(format: "publish == %@",true)
                    self.deleteAllFromDatabase(predicate, allDelete: true)
                    self.existingBondsAddingWork()
                })
                
                alert.addAction(yesAddThem)
                alert.addAction(NoDont)
                
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                self.existingBondsAddingWork()
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }

    
    func existingBondsAddingWork()
    {
        let existing = self.arrForExistingBonds["existingBonds"] as! NSArray
        
        if(existing.count > 0)
        {
            for index in 0..<existing.count
            {
                addBond(existing[index]["denomination_value"] as! String, demoninationId: Int(existing[index]["denomination_id"] as! String)! , id: 1, name: existing[index]["bond_number"] as! String, publish: 0, Sync: 1)
                print(existing[index]["bond_number"] as! String)
            }
        }
        NSUserDefaults.standardUserDefaults().setValue("loggedIn", forKey: "loggedIn")
        NSUserDefaults.standardUserDefaults().setObject(self.tempUserID, forKey: "userID")
        performSegueWithIdentifier("pinVerified", sender: self)
    }
    
    func addBond(denominationValue: String, demoninationId: NSNumber, id: NSNumber,name: String, publish: NSNumber, Sync: NSNumber)
    {
        let entity =  NSEntityDescription.entityForName("Numbers", inManagedObjectContext:managedObjectContext)
        let person = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext: managedObjectContext)
        
        print(denominationValue ,  demoninationId , id , name , publish , Sync)
        
        person.setValue(denominationValue, forKey: "denominationValue")
        person.setValue(demoninationId, forKey: "denominationId")
        person.setValue(id, forKey: "id")
        person.setValue(name, forKey: "number")
        person.setValue(publish, forKey: "publish")
        person.setValue(Sync, forKey: "sync")
        
        people.append(person)
        
        do
        {
            print("bond saved")
            try managedObjectContext.save()
            people.removeAll()
        }
            
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func deleteAllFromDatabase(customPredicate : NSPredicate, allDelete:Bool)
    {
        do
        {
            print(allDelete)
            let fetchRequest = NSFetchRequest(entityName: "Numbers")
            
            if(allDelete == true)
            {
            }
            else
            {
                fetchRequest.predicate = customPredicate
            }
            
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            var deleteableRecords = [NSManagedObject]()
            
            deleteableRecords = results as! [NSManagedObject]
            if deleteableRecords.count > 0 {
                
                for result: AnyObject in deleteableRecords{
                    appDelegate.managedObjectContext.deleteObject(result as! NSManagedObject)
                    print("NSManagedObject has been Deleted")
                }
            }
            try managedObjectContext.save()
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func bondsToDeleteArrayMethod()
    {
        //var predicate = NSPredicate(format: "publish == %@ && denominationValue == %@",true,self.denominationValue)
        CustomFetchForTemporaryUse()
        
        //making of bonds to add array //
        var bondsToDelete = Dictionary<String,AnyObject>()
        bondsToDeleteArray.removeAll()
        
        for i in 0..<uploadArray.count
        {
            let person = uploadArray[i]
            
            print(person.valueForKey("denominationId")!)
            
            bondsToDelete["denomination_id"] = person.valueForKey("denominationId")!
            bondsToDelete["number"] = person.valueForKey("number") as? String
            bondsToDeleteArray.append(bondsToDelete)
        }
        do
        {
            postingArray = ["user_id": NSUserDefaults.standardUserDefaults().valueForKey("userID")!, "denomination_id": "1", "file_id": "1", "BondsToAdd": [], "BondsToDelete": bondsToDeleteArray, "lookup": []]
            
            let postingJson : NSData = try NSJSONSerialization.dataWithJSONObject(postingArray, options: [])
            postingString = String.init(data: postingJson, encoding: NSUTF8StringEncoding)!
            
            print("the posting string will be ", postingString)
        }
        catch
        {
            
        }
        
        let stringValue = "bondsArray=\(postingString)"
        
        self.webServiceMethodWithPostBody(stringValue, url: "http://prizebond.avialdo.com/webservices/insert_bond_numbers.php", parseMethod: "doNothing")
        print(postingString)
    }
    
    func CustomFetchForTemporaryUse()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Numbers")
        
        do
        {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            uploadArray = results as! [NSManagedObject]
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
}
