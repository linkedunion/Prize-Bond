//
//  CustomViewController.swift
//  illustration
//
//  Created by Admin on 8/8/16.
//  Copyright © 2016 Centegy Technologies. All rights reserved.
//

import UIKit
import Foundation

class CustomViewController: UIViewController, UITextFieldDelegate{

    var screen : CGFloat!
    let screenSize: CGRect = UIScreen.mainScreen().bounds
    var screenWidth : CGFloat = 0
    var screenHeight : CGFloat = 0

    let myCustomView = UIView()
    let myactivityIndicatorView = UIActivityIndicatorView()
    
    let whiteProcessCustomView = UIView()
    let processingLabel = UILabel()
    let processingActivityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        screen = self.view.frame.origin.y
        
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        myCustomView.frame = CGRectMake(0, 0, 0, 0)
        myCustomView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        
        myactivityIndicatorView.frame = CGRectMake(screenHeight, 0, 0, 0)
        myactivityIndicatorView.color = UIColor.lightGrayColor()
        myactivityIndicatorView.backgroundColor = UIColor.darkGrayColor()
        
        self.view.addSubview(myCustomView)
        self.view.addSubview(myactivityIndicatorView)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(self.view.frame.origin.y == screen)
        {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()
            {
                self.view.frame.origin.y -= (keyboardSize.height - 90)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        self.view.frame.origin.y = screen
    }
    
    func hideBlackScreen()
    {
        myactivityIndicatorView.frame = CGRectMake(0, 0, 0, 0)
        myCustomView.frame = CGRectMake(0, 0, 0, 0)
        myCustomView.hidden = true
        myactivityIndicatorView.stopAnimating()
    }
    
    func showBlackScreen(showTime:Bool!, specialTime:Bool!)
    {
        myactivityIndicatorView.frame = CGRectMake((self.view.frame.width / 2) - 25, (self.view.frame.height / 2) - 25, 50, 50)
        myCustomView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        myCustomView.hidden = false
        
        if(showTime == true)
        {
            myactivityIndicatorView.hidden = false
            myactivityIndicatorView.startAnimating()
            myCustomView.addSubview(myactivityIndicatorView)
        }
        
        if(specialTime == true)
        {
            showProcessSpecialView()
        }
        self.view.bringSubviewToFront(myCustomView)
    }
    
    
    func showProcessSpecialView()
    {
        whiteProcessCustomView.frame = CGRectMake((self.view.frame.width / 2) - 100, (self.view.frame.height / 2) - 40 , 200, 80)
        whiteProcessCustomView.backgroundColor = UIColor.whiteColor()
        
        
        processingLabel.frame = CGRectMake(whiteProcessCustomView.frame.origin.x + 80, whiteProcessCustomView.frame.origin.y + 30 , 100, 20)
        processingLabel.textColor = UIColor.blackColor()
        processingLabel.text = "Processing"
        
        
        processingActivityIndicator.frame = CGRectMake(whiteProcessCustomView.frame.origin.x + 30, whiteProcessCustomView.frame.origin.y + 30 , 20, 20)
        processingActivityIndicator.color = UIColor.blackColor()
        processingActivityIndicator.startAnimating()
        
        myCustomView.addSubview(whiteProcessCustomView)
        myCustomView.addSubview(processingLabel)
        myCustomView.addSubview(processingActivityIndicator)
    
    }
    
    func hideProcessSpecialView()
    {
        processingActivityIndicator.removeFromSuperview()
        processingLabel.removeFromSuperview()
        whiteProcessCustomView.removeFromSuperview()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func showAlertBoxWithTitle(title:String? , customMessage:String?) -> Void
    {
        if presentedViewController == nil
        {
            let alertController = UIAlertController(title: title, message: customMessage, preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) { (alertAction) -> Void in
                
            }
            
            alertController.addAction(okAction)
            presentViewController(alertController, animated: true, completion: nil)
        }
    }
}

