//
//  Constants.swift
//  Eepsee
//
//  Created by Avialdo on 10/02/2016.
//  Copyright © 2016 Avialdo. All rights reserved.
//

import Foundation
import UIKit

class Constants
{
    
     static var baseRef = "http://128.1.26.149:9093/iportal/"
     static var captchaURl = "http://128.1.26.149:9093/user/sendcaptcha"
     static var loginURL = "http://128.1.26.149:9093/user/login"
    
    // static var baseRef = "http://192.168.171.47:7001/MOBAPP_UAT/iportal/"
    // static var captchaURl = "http://192.168.171.47:7001/MOBAPP_UAT/user/sendcaptcha"
    // static var loginURL = "http://192.168.171.47:7001/MOBAPP_UAT/user/login"
    
    // Deployed at Adamjee
    // static var baseRef = "http://192.168.171.47:7001/MOBAPP_UAT/iportal/"
    // static var captchaURl = "http://192.168.171.47:7001/MOBAPP_UAT/user/sendcaptcha"
    // static var loginURL = "http://192.168.171.47:7001/MOBAPP_UAT/user/login"
    
    // kamran bhai latest with weblogic
//     static var baseRef = "http://128.1.102.8:7001/IPortalManager/iportal/"
//     static var captchaURl = "http://128.1.102.8:7001/IPortalManager/user/sendcaptcha"
//     static var loginURL = "http://128.1.102.8:7001/IPortalManager/user/login"
    
//    static var baseRef = "http://128.1.26.149:8080/IPortal_Manager/iportal/"
//    static var captchaURl = "http://128.1.26.149:8080/IPortal_Manager/user/sendcaptcha"
//    static var loginURL = "http://128.1.26.149:8080/IPortal_Manager/user/login"
    
    
    static var customerDetailsPath = "customer?"
    static var policyDetailsPath = "policies?"
    static var summaryPath = "summary?"
    static var itemDetailsPath = "itemdetail?"
    static var alterationsPath = "alterations?"
    static var endorsementPath = "endorsement?"
    static var policyMembersPath = "policymembers?"
    static var fundsPath = "fundcenters?"
    static var addSettlement = "addsettlmentint"
    static var addAlteration = "addalteration"
    static var addClaim = "addclaim"
    static var claim = "claims?"
    static var coverage = "coverage?"
    static var subCoverage = "subcoverdetails?"
    static var otherDetails = "otherdetail?"
    static var paymentDetails = "paymentdetail?"
    static var settlementDetails = "settlement?"
    static var settlementSubDetail = "settlementdetl?"
    static var fundDetails = "fundsdetails?"
    
    
    static var unsavedDataMessage = "Please save the data to proceed"
    static var fillAndSave = "Please fill and save the data to proceed"
    static var errorMessage = "Something went wrong! Please contact Administrator"
    static var mandatoryFieldsMessage = "Please enter all mandatory information"
    static var alreadyTransferredMessage = "This policy is transferred and cannot be updated."
    static var unsavedBeforeCalculation = "Please fill and save the data before calculation."
    
    static let tabHeadingForApp1 = [["alterations", "claims", "collections", "unitaccount" , "payments", "beneficiary"],["claims", "collections"],["claims"]]

    
    static let sideBarHeadings = [["Inquery", "altchangereq", "claimInti", "settleInti" , "fundcenters"],["Inquery", "altchangereq", "claimInti"],["Inquery"]]
    
    static let sideBarSegues = ["Inquery": "toPoliciesVC", "altchangereq": "toPoliciesVC", "claimInti": "toPoliciesVC", "settleInti" : "toPoliciesVC", "fundcenters": "toFundsScreen"]
    
    
    static let sideBarTitles = ["", "Customer Profile", "Inquiry", "Alteration Change Requests", "Claim Intimation Requests", "Settlement Intimation Requests" , "Fund Center","LogOut"]
    
    
    static var componentLabelTextColor : UIColor!
    static var componentComboBgDisableColor : UIColor!
    static var cameraButtonsBorderColor : UIColor!
    static var headerAndSpecialViewsColor : UIColor!
    static var allButtonsTextColor : UIColor!
    
    static func setTheme(themeName:String)
    {
        if(themeName == "Red")
        {
            componentLabelTextColor = UIColor(red: 69/255, green: 69/255, blue: 69/255, alpha: 1.0)
            componentComboBgDisableColor = UIColor(red: 208/255, green: 215/255, blue: 239/255, alpha: 1.0)
            cameraButtonsBorderColor = UIColor(red: 202/255, green: 202/255, blue: 202/255, alpha: 1.0)
            headerAndSpecialViewsColor = UIColor(red: 202/255, green: 202/255, blue: 202/255, alpha: 1.0)
            allButtonsTextColor = UIColor(red: 0/255, green: 166/255, blue: 183/255, alpha: 1.0)
        }
        else if(themeName == "Green")
        {
//            componentLabelTextColor = UIColor(red: 69/255, green: 69/255, blue: 69/255, alpha: 1.0)
//            componentComboBgDisableColor = UIColor(red: 208/255, green: 215/255, blue: 239/255, alpha: 1.0)
//            headerAndSpecialViewsColor = UIColor(red: 17/255, green: 165/255, blue: 87/255, alpha: 1.0)
//            cameraButtonsBorderColor = UIColor(red: 202/255, green: 202/255, blue: 202/255, alpha: 1.0)
            allButtonsTextColor = UIColor(red: 0/255, green: 102/255, blue: 0/255, alpha: 1.0)
            
            //green theme navigation bar color , R = 0 G= 102 B = 0
        }
        else
        {
            componentLabelTextColor = UIColor(red: 69/255, green: 69/255, blue: 69/255, alpha: 1.0)
            componentComboBgDisableColor = UIColor(red: 208/255, green: 215/255, blue: 239/255, alpha: 1.0)
            headerAndSpecialViewsColor = UIColor(red: 0/255, green: 166/255, blue: 183/255, alpha: 1.0)
            cameraButtonsBorderColor = UIColor(red: 202/255, green: 202/255, blue: 202/255, alpha: 1.0)
            allButtonsTextColor = UIColor(red: 0/255, green: 166/255, blue: 183/255, alpha: 1.0)
            
            //blue theme navigation bar color , R = 62 G= 93 B = 175
        }
    }
    
    enum ThemeColor : String
    {
        case Green, Red, Blue
    }
}