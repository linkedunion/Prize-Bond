//
//  PrizeBondViewController.swift
//  prize_bond
//
//  Created by Avialdo on 12/11/2015.
//  Copyright © 2015 Avialdo. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON

class PrizeBondViewController: CustomViewController, UITableViewDataSource,UITableViewDelegate,UIPopoverPresentationControllerDelegate, prizeBondPopOverViewControllerDelegate,bondSearchPopOverViewControllerDelegate{
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var arrDates = [[String:AnyObject]]()
    var timeToCallWebservice = true
    var isSearchDisplay : Bool!
    var denominationId : String!
    var denomiationArray : [[String]] = [["1","100"],["2","200"],["3","750"],["4","1500"],["5","7500"],["6","15000"],["7","25000"],["8","40000"]]
    
    var tempTextFieldtext: String!
    @IBOutlet weak var bondNumberTextField: UITextField!
    @IBOutlet weak var range_gap: UILabel!
    @IBOutlet weak var range_first: UITextField!
    @IBOutlet weak var range_second: UITextField!
    @IBOutlet weak var single_entry: UITextField!
    
    @IBOutlet weak var bondsTableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    @IBOutlet weak var clickButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var drawsLabel: UILabel!
    @IBOutlet weak var searchFromAllLabel: UILabel!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var draw_date_textfield: UITextField!
    @IBOutlet weak var clickkButton: UIButton!
    var searchDrawTextId : String!
    
    var postingArray = [String:AnyObject]()
    var bondsToAddArray = [AnyObject]()
    var bondsToDeleteArray = [AnyObject]()
    var bondsToLookupArray = [AnyObject]()
    var postingString : String!
    var arrRes = [String:AnyObject]()
    
    var arrayForTableView = [String:AnyObject]()
    var people = [NSManagedObject]()
    
    @IBOutlet weak var hiddenButton: UIButton!
    @IBOutlet weak var hiddenButtonTop: UIButton!
    
    @IBAction func checkBoxAction(sender: AnyObject)
    {
        if(checkBoxButton.selected)
        {
            checkBoxButton.selected = false
            clickkButton.enabled = true
            draw_date_textfield.text = ""
            hiddenButton.enabled = true
            checkBoxButton.setBackgroundImage(UIImage(named: "unchecked_checkbox"), forState: UIControlState.Normal)
        }
        else
        {
            draw_date_textfield.text = "All Draws"
            searchDrawTextId = "0"
            clickkButton.enabled = false
            hiddenButton.enabled = false
            checkBoxButton.selected = true
            checkBoxButton.setBackgroundImage(UIImage(named: "checked_checkbox"), forState: UIControlState.Normal)
        }
    }
    
    @IBAction func clickButtonAction(sender: AnyObject)
    {
        performSegueWithIdentifier("searchPopOverSegueFromAdd", sender: sender)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator.hidden = true
        
        print(self.bondNumberTextField.text)
        addToolBar(single_entry)
        addToolBar(range_first)
        addToolBar(range_second)
        
        self.range_gap.hidden = true
        self.range_first.hidden = true
        self.range_second.hidden = true
        
        if(tempTextFieldtext != nil)
        {
            bondNumberTextField.text = tempTextFieldtext
        }
        else
        {
            self.bondNumberTextField.text = "100"
        }
        
        self.bondsTableView.layer.borderColor = UIColor.grayColor().CGColor
        self.bondsTableView.layer.borderWidth = 1.0
        self.bondsTableView.tableFooterView = UIView()
        
        print("the search value is" , isSearchDisplay!)
        
        if(isSearchDisplay == true)
        {
            searchButton.hidden = false
            saveButton.enabled = false
            saveButton.hidden = true
            clickkButton.enabled = false
            hiddenButton.enabled = false
            self.title = "Search Bonds"
        }
        else
        {
            searchButton.hidden = true
            saveButton.hidden = false
            searchButton.enabled = false
            self.title = "Add Bonds"
            hideExtraThings()
        }
        
        if(NSUserDefaults.standardUserDefaults().objectForKey("userID") == nil)
        {
            sendUDIDandGetUserID()
        }
        
        //fetchData()
        
    }
    
    func sendUDIDandGetUserID()
    {
        let myUdidValue :String = NSUserDefaults.standardUserDefaults().objectForKey("UDID")! as! String
        let postBodyString = "UDID=\(myUdidValue)"
        
        print(postBodyString)
        
        Alamofire.request(.POST,"http://prizebond.avialdo.com/webservices/register_udid.php", parameters: [:],  encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.HTTPBody = postBodyString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:  false)
            return (mutableRequest, nil)
        }), headers: nil).responseJSON { (responseData) -> Void in
            
            if(responseData.2.value != nil)
            {
                let swiftyJsonVar = JSON(responseData.2.value!)
                if let resData = swiftyJsonVar[].dictionaryObject {
                    self.arrRes = resData
                }
                if self.arrRes.count > 0
                {
                    NSUserDefaults.standardUserDefaults().setObject(self.arrRes["user_id"]!, forKey: "userID")
                }
            }
            else
            {
                
                self.showAlertBoxWithTitle("Network Failure", customMessage: "There is some problem with your internet. Please try again later.")
            }
        }
    }
    
    func selectBondNumber(bondNumber: Int)
    {
        for i in 0..<denomiationArray.count
        {
            if(Int(denomiationArray[i][0]) == bondNumber)
            {
                self.bondNumberTextField.text = denomiationArray[i][1]
            }
        }
    }
    
    func findDenominationId(bondNumber: String) -> Int
    {
        var tempDenominationId : Int!
        for i in 0..<denomiationArray.count
        {
            if(denomiationArray[i][1] == bondNumber)
            {
                tempDenominationId = Int(denomiationArray[i][0])
                break
            }
        }
        return tempDenominationId
    }
    
    
    @IBAction func add_bond(sender: AnyObject)
    {
        if(segmentedControl.selectedSegmentIndex == 0)
        {
            if(self.single_entry.text! != "")
            {
                if(self.single_entry.text?.characters.count < 6)
                {
                    self.showAlertBoxWithTitle("Sorry", customMessage: "Please enter 6 digit bond number")
                }
                else if(self.single_entry.text! == "000000")
                {
                    self.showAlertBoxWithTitle("Sorry", customMessage: "The bond number you have entered is not valid")
                }
                else
                {
                    addBond(self.bondNumberTextField.text!, demoninationId: findDenominationId(self.bondNumberTextField.text!), id: 0, name: self.single_entry.text!, publish: 0, Sync: 0)
                    self.single_entry.text! = ""
                    
                    if(timeToCallWebservice == true && isSearchDisplay == true)
                    {
                        self.callingDateWebService()
                        activityIndicator.hidden = false
                        activityIndicator.startAnimating()
                    }
                    
                    if(isSearchDisplay == true)
                    {
                        clickButton.enabled = false
                        hiddenButtonTop.enabled = false
                    }
                }
                
            }
        }
                                                                ////// For range ////////
        else
        {
            if(self.range_first.text! != "" || self.range_second.text != "")
            {
                if(self.range_first.text?.characters.count < 6 || self.range_second.text?.characters.count < 6)
                {
                    self.showAlertBoxWithTitle("Sorry", customMessage: "Please enter 6 digit bond number")
                }
                else
                {
                    if(Int(self.range_second.text!)! >= Int(self.range_first.text!)!)
                    {
                        if((Int(self.range_second.text!)! - Int(self.range_first.text!)!) <= 500)
                        {
                            for i in Int(self.range_first.text!)!...Int(self.range_second.text!)!
                            {
                                var bondNumber = String(i)
                                
                                if(bondNumber.characters.count < 6)
                                {
                                    for zeros in 0..<(6 - bondNumber.characters.count)
                                    {
                                        bondNumber = "0" + bondNumber
                                    }
                                }
                                if(bondNumber == "000000")
                                {
                                    
                                }
                                else
                                {
                                    if(timeToCallWebservice == true)
                                    {
                                        self.callingDateWebService()
                                        self.timeToCallWebservice = false
                                        activityIndicator.hidden = false
                                        activityIndicator.startAnimating()
                                    }
                                    
                                    addBond(self.bondNumberTextField.text!, demoninationId: findDenominationId(self.bondNumberTextField.text!), id: 0, name: bondNumber, publish: 0, Sync: 0)
                                    self.range_first.text! = ""
                                    self.range_second.text! = ""
                                    
                                    if(isSearchDisplay == true)
                                    {
                                        clickButton.enabled = false
                                        hiddenButtonTop.enabled = false
                                    }
                                }
                            }
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Sorry", message: "You can only insert 500 bonds at a time", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                            
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Sorry", message: "Please insert a valid range", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                        
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    @IBAction func segment_control(sender: AnyObject)
    {
        if(segmentedControl.selectedSegmentIndex == 0)
        {
            self.range_gap.hidden = true
            self.range_first.hidden = true
            self.range_second.hidden = true
            self.single_entry.hidden = false
        }
        else if(segmentedControl.selectedSegmentIndex == 1)
        {
            self.range_gap.hidden = false
            self.range_first.hidden = false
            self.range_second.hidden = false
            self.single_entry.hidden = true
        }
    }
    
    func fetchData()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Numbers")
        
        do
        {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            people = results as! [NSManagedObject]
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }

    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return people.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("bondCell", forIndexPath: indexPath) as! PrizeBondTableViewCell
        
        let person = people[indexPath.row]
        let myDenominationValue = person.valueForKey("denominationValue") as! String
        
        cell.bond_number!.text = person.valueForKey("number") as? String
        cell.bond_denomination!.text = "Rs \(myDenominationValue)"
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            
            managedObjectContext.deleteObject(people[indexPath.row] as NSManagedObject)
            people.removeAtIndex(indexPath.row)
            do {
                try managedObjectContext.save()
                
            } catch let error as NSError
            {
                print("Could not save \(error), \(error.userInfo)")
            }
            
            if(tableView.numberOfRowsInSection(0) == 1)
            {
                clickButton.enabled = true
                hiddenButtonTop.enabled = true
                self.draw_date_textfield.text = ""
                self.timeToCallWebservice = true
                self.clickkButton.enabled = false
                self.hiddenButton.enabled = false
            }
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    
    
    func addBond(denominationValue: String, demoninationId: NSNumber, id: NSNumber,name: String, publish: NSNumber, Sync: NSNumber)
    {
        let entity =  NSEntityDescription.entityForName("Numbers", inManagedObjectContext:managedObjectContext)
        let person = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext: managedObjectContext)
        
        person.setValue(denominationValue, forKey: "denominationValue")
        person.setValue(demoninationId, forKey: "denominationId")
        person.setValue(id, forKey: "id")
        person.setValue(name, forKey: "number")
        person.setValue(publish, forKey: "publish")
        person.setValue(Sync, forKey: "sync")
        
        people.append(person)
        self.bondsTableView.reloadData()
    }
    
    @IBAction func saveButton(sender: AnyObject)
    {
        if(people.count > 0)
        {
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want to save these bonds?", preferredStyle: UIAlertControllerStyle.Alert)
            
            let yesAddThem = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                do
                {
                    self.updateValue("id", changingValue: 1)
                    
                    try self.managedObjectContext.save()
                    self.people.removeAll()
                    self.bondsTableView.reloadData()
                }
                    
                catch let error as NSError
                {
                    print("Could not fetch \(error), \(error.userInfo)")
                }
                
            })
            
            let NoDont = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
            })
            
            alert.addAction(yesAddThem)
            alert.addAction(NoDont)
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteBond(sender: AnyObject)
    {
        if(people.count > 0)
        {
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want to delete all these bonds?", preferredStyle: UIAlertControllerStyle.Alert)
            
            let yesAddThem = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                self.people.removeAll()
                self.bondsTableView.reloadData()
                self.clickButton.enabled = true
                self.hiddenButtonTop.enabled = true
                self.timeToCallWebservice = true
                self.draw_date_textfield.text = ""
                self.clickkButton.enabled = false
                self.hiddenButton.enabled = false
                
            })
            
            let NoDont = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
            })
            
            alert.addAction(yesAddThem)
            alert.addAction(NoDont)
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func popoverControllerDidDismissPopover(popoverController: UIModalPresentationStyle) {
        
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    @IBAction func animationClick(sender: AnyObject)
    {
        performSegueWithIdentifier("prizeBondPopOverSegue", sender: sender)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "prizeBondPopOverSegue")
        {
            let popOver = segue.destinationViewController as! prizeBondPopOverViewController
            popOver.pDelegate = self
            popOver.modalPresentationStyle = .Popover
            popOver.preferredContentSize = CGSize(width: 260, height: 350)
            popOver.view.frame = self.view.frame
            let controller = popOver.popoverPresentationController
            if controller != nil {
                controller?.delegate = self
                controller?.permittedArrowDirections = UIPopoverArrowDirection()
                controller?.sourceRect = self.view.frame
                controller?.sourceView = self.view
                controller?.sourceRect.origin = self.view.frame.origin
            }
        }
        else if(segue.identifier == "searchPopOverSegueFromAdd")
        {
            for i in 0..<denomiationArray.count
            {
                if(denomiationArray[i][1] == self.bondNumberTextField.text!)
                {
                    self.denominationId = denomiationArray[i][0]
                }
            }
            
            let popOver = segue.destinationViewController as! bondSearchPopOverViewController
            popOver.pDelegate = self
            popOver.denominationId = self.denominationId
            popOver.timeToCallWebservice = self.timeToCallWebservice
            popOver.arrRes = self.arrDates
            popOver.modalPresentationStyle = .Popover
            popOver.preferredContentSize = CGSize(width: 200, height: 300)
            popOver.view.frame = self.view.frame
            let controller = popOver.popoverPresentationController
            if controller != nil {
                controller?.delegate = self
                controller?.permittedArrowDirections = UIPopoverArrowDirection()
                controller?.sourceRect = self.view.frame
                controller?.sourceView = self.view
                controller?.sourceRect.origin = self.view.frame.origin
            }
        }
        else if(segue.identifier == "resultScreenSegue")
        {
            let resultScreen = segue.destinationViewController as! ResultViewController
            resultScreen.ResultArray = self.arrRes
            resultScreen.denominationValue = self.bondNumberTextField.text
        }
    }
    
    @IBAction func searchButtonClicked(sender: AnyObject)
    {
        if(people.count > 0)
        {
            var bondsToLookup = Dictionary<String,String>()
            bondsToLookupArray.removeAll()
            
            for i in 0..<denomiationArray.count
            {
                //if(Int(denomiationArray[i][1]) isE
                if(denomiationArray[i][1] == self.bondNumberTextField.text!)
                {
                    self.denominationId = denomiationArray[i][0]
                }
            }
            
            for i in 0..<people.count
            {
                let person = people[i]
                
                bondsToLookup["bond_number"] = person.valueForKey("number") as? String
                bondsToLookupArray.append(bondsToLookup)
            }
            
            if(draw_date_textfield.text != "")
            {
                do
                {
                    postingArray = ["user_id": NSUserDefaults.standardUserDefaults().valueForKey("userID")!, "denomination_id": denominationId, "file_id": searchDrawTextId, "BondsToAdd":bondsToAddArray,"BondsToDelete": bondsToDeleteArray, "lookup": bondsToLookupArray]
                    
                    let postingJson : NSData = try NSJSONSerialization.dataWithJSONObject(postingArray, options: [])
                    postingString = String.init(data: postingJson, encoding: NSUTF8StringEncoding)!
                }
                catch
                {
                    
                }
                sendAllThreeArrays()
                print(postingString)
            }
            
            else
            {
                showAlertBoxWithTitle("Sorry", customMessage: "Please select a search date first")
            }
        }
        else
        {
            showAlertBoxWithTitle("Sorry", customMessage: "You have no bonds to search")
        }
    }
    
    func sendAllThreeArrays()
    {
        let stringValue = "bondsArray=\(postingString)"
        Alamofire.request(.POST,"http://prizebond.avialdo.com/webservices/insert_bond_numbers.php", parameters: [:], encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.HTTPBody = stringValue.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion:  false)
            return (mutableRequest, nil)
        }), headers: nil).responseJSON { (responseData) -> Void in
            if(responseData.2.value != nil)
            {
                let swiftyJsonVar = JSON(responseData.2.value!)
                if let resData = swiftyJsonVar[].dictionaryObject {
                    self.arrRes = resData
                }
                if self.arrRes.count > 0
                {
                    self.performSegueWithIdentifier("resultScreenSegue", sender: self)
                }
            }
            else
            {
                
                self.showAlertBoxWithTitle("Network Failure", customMessage: "There is some problem with your internet. Please try again later.")
            }
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {

        if ((textField.text?.characters.count) >= 6  && range.length == 0)
        {
            return false // return NO to not change text
        }
        else
        {
            return true
        }
    }

    func hideExtraThings()
    {
        draw_date_textfield.hidden = true
        drawsLabel.hidden = true
        clickkButton.hidden = true
        searchFromAllLabel.hidden = true
        checkBoxButton.hidden = true
    }
    
    func selectDate(date: String, drawID: String, array:[[String:AnyObject]]) {
        draw_date_textfield.text = date
        searchDrawTextId = drawID
        timeToCallWebservice = false
    }
    
    func addToolBar(textField: UITextField){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 186/255, blue: 210/255, alpha: 1)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: #selector(PrizeBondViewController.donePressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        toolBar.sizeToFit()
        
        textField.delegate = self
        textField.inputAccessoryView = toolBar
    }
    
    func donePressed(){
        view.endEditing(true)
    }
    
    func updateValue(changingKey: String, changingValue: Int)
    {
        do
        {
            if(people.count>0)
            {
                for i in 0..<people.count
                {
                    let person = people[i]
                    person.setValue(changingValue, forKey: changingKey)
                    do {
                        try person.managedObjectContext?.save()
                    } catch {
                        let saveError = error as NSError
                        print(saveError)
                    }
                    
                }
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    
    func callingDateWebService()
    {
        denominationId = String(findDenominationId(self.bondNumberTextField.text!))

        let postBodyString = "denomination_id=\(denominationId)"
        
        Alamofire.request(.POST,"http://prizebond.avialdo.com/webservices/get_draw_list.php", parameters: [:], encoding: .Custom({
            (convertible, params) in
            let mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
            mutableRequest.HTTPBody = postBodyString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
            return (mutableRequest, nil)
        }), headers: nil).responseJSON { (responseData) -> Void in
            
            if (responseData.2.value != nil)
            {
                let swiftyJsonVar = JSON(responseData.2.value!)
                if let resData = swiftyJsonVar[].arrayObject
                {
                    self.arrDates = resData as! [[String:AnyObject]]
                }
                if self.arrDates.count > 0
                {
                    if(self.people.count > 0)
                    {
                        // to check the condition in which user has deleted the bonds before the response of date webservice.
                        
                        self.timeToCallWebservice = false
                        var dict = self.arrDates[0]
                        self.draw_date_textfield.text = dict["draw_date"] as? String
                        self.searchDrawTextId = dict["id"] as? String
                        
                        self.clickkButton.enabled = true
                        self.hiddenButton.enabled = true
                    }
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.hidden = true
                }
            }
            else
            {
                self.showAlertBoxWithTitle("Network Failure", customMessage: "There is some problem with your internet. Please try again later.")
                self.activityIndicator.stopAnimating()
                self.activityIndicator.hidden = true
            }
        }
    }
}
