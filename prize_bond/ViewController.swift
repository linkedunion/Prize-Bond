//
//  ViewController.swift
//  prize_bond
//
//  Created by Avialdo on 11/11/2015.
//  Copyright © 2015 Avialdo. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController
{
    var denominationId : String!
    var denominationValue : String!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let fetchRequest = NSFetchRequest(entityName: "Numbers")
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    var bondsArrayCount : [[Int]] = [[0 , 100],[0 , 200],[0 , 750],[0 , 1500],[0 , 7500],[0 , 15000],[0 , 25000],[0 , 40000]]
    
    @IBOutlet weak var label100: UILabel!
    @IBOutlet weak var label200: UILabel!
    @IBOutlet weak var label750: UILabel!
    @IBOutlet weak var label1500: UILabel!
    @IBOutlet weak var label7500: UILabel!
    @IBOutlet weak var label15000: UILabel!
    @IBOutlet weak var label25000: UILabel!
    @IBOutlet weak var label40000: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
        
        getBondsCount()
        displayBonds()
    }
    
    @IBOutlet var buttons: [UIButton]!
    

    @IBAction func plusButtonClicked(sender: AnyObject)
    {
        
        let optionMenu = UIAlertController(title: nil, message: "What you want to do", preferredStyle: .ActionSheet)
        
        let addbonds = UIAlertAction(title: "Add Bonds", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.denominationId = "1"
            self.performSegueWithIdentifier("addBondSegue", sender: sender)
        })
        
        let drawSchedule = UIAlertAction(title: "Draw Schedule", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.performSegueWithIdentifier("drawListSegue", sender: sender)
        })
        
        var signOutOrRegister : UIAlertAction!
        
        if(NSUserDefaults.standardUserDefaults().valueForKey("loggedIn") != nil)
        {
            signOutOrRegister = UIAlertAction(title: "Sign Out", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                NSUserDefaults.standardUserDefaults().removeObjectForKey("loggedIn")
                NSUserDefaults.standardUserDefaults().removeObjectForKey("userID")
                self.deleteFromDatabase()
                self.performSegueWithIdentifier("againLogIn", sender: sender)
            })
        }
        else
        {
            signOutOrRegister = UIAlertAction(title: "Register / Sign In", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                self.performSegueWithIdentifier("againLogIn", sender: sender)
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(addbonds)
        optionMenu.addAction(drawSchedule)
        optionMenu.addAction(signOutOrRegister)
        optionMenu.addAction(cancelAction)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func hundredClicked(sender: AnyObject)
    {
        denominationId = "1"
        denominationValue = "100"
        performSegueWithIdentifier("searchSegue", sender: sender)
    }
    @IBAction func twoHundredClicked(sender: AnyObject)
    {
        denominationId = "2"
        denominationValue = "200"
        performSegueWithIdentifier("searchSegue", sender: sender)
    }
    @IBAction func sevenFiftyclicked(sender: AnyObject)
    {
        denominationId = "3"
        denominationValue = "750"
        performSegueWithIdentifier("searchSegue", sender: sender)
    }
    @IBAction func fifteenHundredClicked(sender: AnyObject)
    {
        denominationId = "4"
        denominationValue = "1500"
        performSegueWithIdentifier("searchSegue", sender: sender)
    }
    @IBAction func fifteenThousndClicked(sender: AnyObject)
    {
        denominationId = "6"
        denominationValue = "15000"
        performSegueWithIdentifier("searchSegue", sender: sender)
    }
    @IBAction func sevenThousandFiveHundredClicked(sender: AnyObject)
    {
        denominationId = "5"
        denominationValue = "7500"
        performSegueWithIdentifier("searchSegue", sender: sender)
    }
    @IBAction func twentyFiveThousandClicked(sender: AnyObject)
    {
        denominationId = "7"
        denominationValue = "25000"
        performSegueWithIdentifier("searchSegue", sender: sender)
    }
    @IBAction func fortyThousandClicked(sender: AnyObject)
    {
        denominationId = "8"
        denominationValue = "40000"
        performSegueWithIdentifier("searchSegue", sender: sender)
    }
    @IBAction func quickLookUp(sender: AnyObject)
    {
        denominationValue = "0"
        performSegueWithIdentifier("quickLookupSegue", sender: sender)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "addBondSegue")
        {
            if let prizebondClass = segue.destinationViewController as? PrizeBondViewController
            {
                prizebondClass.denominationId = self.denominationId
                prizebondClass.isSearchDisplay = false
                print("the search value is ",prizebondClass.isSearchDisplay)
            }
        }
        if(segue.identifier == "searchSegue")
        {
            if let searchBondClass = segue.destinationViewController as? searchViewController
            {
                searchBondClass.denominationId = self.denominationId
                searchBondClass.denominationValue = self.denominationValue
                print("value in view controller",searchBondClass.denominationValue)
            }
        }
        if(segue.identifier == "quickLookupSegue")
        {
            if let prizebondClass = segue.destinationViewController as? PrizeBondViewController
            {
                prizebondClass.isSearchDisplay = true
                print("the search value is ",prizebondClass.isSearchDisplay)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    func deleteFromDatabase()
    {
        do
        {
            let fetchRequest = NSFetchRequest(entityName: "Numbers")
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            var deleteableRecords = [NSManagedObject]()
            
            deleteableRecords = results as! [NSManagedObject]
            if deleteableRecords.count > 0 {
                
                for result: AnyObject in deleteableRecords{
                    appDelegate.managedObjectContext.deleteObject(result as! NSManagedObject)
                    print("NSManagedObject has been Deleted")
                }
            }
            try managedObjectContext.save()
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func getBondsCount()
    {
        for countIndex in 0..<bondsArrayCount.count
        {
            bondsArrayCount[countIndex][0] = 0
        }
        
        do
        {
            let predicate = NSPredicate(format: "id == %@ && publish == %@", "1", "0")
            fetchRequest.predicate = predicate
            
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            var bonds = [NSManagedObject]()
            
            bonds = results as! [NSManagedObject]
            if bonds.count > 0
            {
                for index in 0..<bonds.count
                {
                    let person = bonds[index]
                    
                    for countIndex in 0..<bondsArrayCount.count
                    {
                        if(bondsArrayCount[countIndex][1] == Int(person.valueForKey("denominationValue") as! String))
                        {
                            bondsArrayCount[countIndex][0] += 1
                            break
                        }
                    }
                }

            }
            try managedObjectContext.save()
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func displayBonds()
    {
        self.label100.text =  "\(bondsArrayCount[0][0])"
        self.label200.text =  "\(bondsArrayCount[1][0])"
        self.label750.text =  "\(bondsArrayCount[2][0])"
        self.label1500.text =  "\(bondsArrayCount[3][0])"
        self.label7500.text =  "\(bondsArrayCount[4][0])"
        self.label15000.text =  "\(bondsArrayCount[5][0])"
        self.label25000.text =  "\(bondsArrayCount[6][0])"
        self.label40000.text =  "\(bondsArrayCount[7][0])"
    }
}

