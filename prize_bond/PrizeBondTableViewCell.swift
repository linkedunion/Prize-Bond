//
//  PrizeBondTableViewCell.swift
//  prize_bond
//
//  Created by Avialdo on 12/11/2015.
//  Copyright © 2015 Avialdo. All rights reserved.
//

import UIKit

class PrizeBondTableViewCell: UITableViewCell {

    @IBOutlet weak var bond_number: UILabel!
    @IBOutlet weak var bond_denomination: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
