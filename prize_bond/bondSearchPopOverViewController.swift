//
//  bondSearchPopOverViewController.swift
//  prize_bond
//
//  Created by Avialdo on 19/11/2015.
//  Copyright © 2015 Avialdo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class bondSearchPopOverViewController: CustomViewController, UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var bondSearchTableView: UITableView!
    
    var timeToCallWebservice : Bool!
    var arrRes = [[String:AnyObject]]()
    var denominationId : String!
    var progressIcon = UIActivityIndicatorView()
    
    var pDelegate :bondSearchPopOverViewControllerDelegate?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrRes.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("bondSearchCell", forIndexPath: indexPath) as! bondSearchTableViewCell
        
        var dict = arrRes[indexPath.row]
        cell.textLabel?.text = dict["draw_date"] as? String
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var dict = arrRes[indexPath.row]
        pDelegate?.selectDate((dict["draw_date"] as? String)!, drawID: (dict["id"] as? String)!, array: self.arrRes)
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}

protocol bondSearchPopOverViewControllerDelegate
{
    func selectDate(date: String , drawID: String , array:[[String:AnyObject]])
    
}
